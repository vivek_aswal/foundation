var express = require('express'),
app = express(),
port = process.env.PORT || 3000,
mongoose = require('mongoose'),

// Model Schema
Task = require('./api/task/todoListModel'),
User = require('./api/user/userModel'),


bodyParser = require('body-parser');

var responseTime = require('response-time');


mongoose.Promise = global.Promise;
mongoose.connect('mongodb://localhost/foundation');

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

var routes = require('./routes/routes');
routes(app);



app.use(function(req, res) {
res.status(404).send({url: req.originalUrl + ' not found'})
});

// set up the response-time middleware
app.use(responseTime());

app.listen(port);

console.log('todo list RESTful API server started on: ' + port);
