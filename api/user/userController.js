'use strict';

var mongoose = require('mongoose'),
  User = mongoose.model('Users');

// Caching
var redis = require('redis');

// create a new redis client and connect to our local redis instance
var client = redis.createClient();

// if an error occurs, print it to the console
client.on('connect', function (err) {
    console.log("Connected to Redis");
});

client.on('error', function (err) {
    console.log("Error " + err);
});


exports.list_all_users = function(req, res) {

client.get('list_all_users', function(error, result) {
        if (result) {
          // the result exists in our cache - return it to our user immediately
          res.send({ "result": result, "source": "redis cache" });
        } else {

          User.find({}, function(err, user) {
            if (err){
              res.send(err);
            }
            client.setex("list_all_users", 20, user);
            res.json(user);
          });
        }
  });
};




exports.create_a_user = function(req, res) {
  var new_user = new User(req.body);
  new_user.save(function(err, user) {
    if (err)
      res.send(err);
    res.json(user);
  });
};

exports.read_a_user = function(req, res) {
  User.findById(req.params.userId, function(err, user) {
    if (err)
      res.send(err);
    res.json(user);
  });
};

exports.update_a_user = function(req, res) {
  User.findOneAndUpdate({_id:req.params.userId}, req.body, {new: true}, function(err, user) {
    if (err)
      res.send(err);
    res.json(user);
  });
};
// User.remove({}).exec(function(){});
exports.delete_a_user = function(req, res) {

  User.remove({
    _id: req.params.userId
  }, function(err, user) {
    if (err)
      res.send(err);
    res.json({ message: 'User successfully deleted' });
  });
};
